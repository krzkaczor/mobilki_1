package com.example.krzkaczor.mobilki_1;

import android.content.Context;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


public class MainActivity extends ActionBarActivity {

    private View indicator;
    private Animation blinkAnimation;
    int state = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.indicator = findViewById(R.id.blinkingView);

        triggerBlink();
    }

    private void triggerBlink() {
        blinkAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        indicator.startAnimation(blinkAnimation);
    }

    private void scheduleTriggerBlink() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        triggerBlink();
                    }
                });

            }
        }, 100);
    }

    public void forestClick(View view){
        if (state != 0) {
            Toast.makeText(this, "Wrong guess!", Toast.LENGTH_SHORT).show();
            return;
        }
        state++;
        moveImageToTarget(view, "forest", findViewById(R.id.forestLabel));
        animateTo(findViewById(R.id.birdLabel));
    }

    public void birdClick(View view){
        if (state != 1) {
            Toast.makeText(this, "Wrong guess!", Toast.LENGTH_SHORT).show();
            return;
        }
        state++;
        moveImageToTarget(view, "bird", findViewById(R.id.birdLabel));
        animateTo(findViewById(R.id.treeLabel));
    }

    public void treeClick(View view){
        if (state != 2) {
            Toast.makeText(this, "Wrong guess!", Toast.LENGTH_SHORT).show();
            return;
        }
        state++;
        moveImageToTarget(view, "tree", findViewById(R.id.treeLabel));
        animateTo(findViewById(R.id.flowersLabel));
    }

    public void flowerClick(View view){
        if (state != 3) {
            Toast.makeText(this, "Wrong guess!", Toast.LENGTH_SHORT).show();
            return;
        }
        state++;
        moveImageToTarget(view, "flowers", findViewById(R.id.flowersLabel));
        animateTo(findViewById(R.id.forestLabel));
        Toast.makeText(this, "YOU WON!", Toast.LENGTH_LONG).show();
    }

    private void animateTo(final View target) {
        blinkAnimation.cancel();
        int targetLocation[] = new int[2];
        target.getLocationOnScreen(targetLocation);

        int indicatorLocation[] = new int[2];
        indicator.getLocationOnScreen(indicatorLocation);

        final int deltaX = targetLocation[0] - indicatorLocation[0];
        final int deltaY = targetLocation[1] - indicatorLocation[1];

        TranslateAnimation translateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, deltaX, Animation.RELATIVE_TO_SELF, deltaY);
        translateAnimation.setDuration(500);
        translateAnimation.setFillEnabled(true);
        indicator.startAnimation(translateAnimation);

        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) indicator.getLayoutParams();
                lp.leftMargin = lp.leftMargin + deltaX;
                lp.topMargin = lp.topMargin + deltaY;
                indicator.setLayoutParams(lp);

                scheduleTriggerBlink();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void moveImageToTarget(View clickedButton, String imageName, View target) {
        RelativeLayout root = (RelativeLayout) findViewById(R.id.free);

        ImageView image = new ImageView(this);
        image.setImageResource(getDrawable(this, imageName));
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);

        int buttonLocation[] = new int[2];
        clickedButton.getLocationOnScreen(buttonLocation);
        int origin[] = new int[2];
        root.getLocationOnScreen(origin);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(clickedButton.getWidth(), clickedButton.getHeight());
        lp.leftMargin = buttonLocation[0] - origin[0];
        lp.topMargin = buttonLocation[1] - origin[1];
        image.setLayoutParams(lp);

        root.addView(image);

        int originalPos[] = new int[2];
        clickedButton.getLocationOnScreen( originalPos );
        int targetPos[] = new int[2];
        target.getLocationOnScreen( targetPos );

        int xDelta = targetPos[0] - originalPos[0];
        int yDelta = targetPos[1] - originalPos[1];

        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillAfter(true);
        animSet.setDuration(500);
//        animSet.setInterpolator(new BounceInterpolator());
        TranslateAnimation translate = new TranslateAnimation(Animation.RELATIVE_TO_SELF, xDelta / 2 , Animation.RELATIVE_TO_SELF, yDelta / 2);
        animSet.addAnimation(translate);
        ScaleAnimation scale = new ScaleAnimation(1f, 2f, 1f, 2f);
        animSet.addAnimation(scale);
        image.startAnimation(animSet);
    }


    public static int getDrawable(Context context, String name)
    {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }
}
